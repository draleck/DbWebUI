import vue from '@vitejs/plugin-vue';
import * as path from 'path';
import {defineConfig, type PluginOption} from 'vite';
import eslintPlugin from 'vite-plugin-eslint';
import visualizer from 'rollup-plugin-visualizer';

// import {minifyHtml} from 'vite-plugin-html';

export default defineConfig({
    plugins: [
        eslintPlugin(),
        vue(),
        {
        ...visualizer({
                     filename: './statistics.html', sourcemap: true, title: 'statistics'
                   })
        } as PluginOption
        // minifyHtml()
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './src')
        }
    },
    server: {
        hmr: { overlay: false },
        port: 8088
    }
});

eslintPlugin({ cache: false });
