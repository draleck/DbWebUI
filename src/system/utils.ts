// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const $$ = {
    lastUniqueId: '',
    uniqueId(baseId = ''): string {
        const maxRandomSize = 6 + 6,
            randomStr = '' + Math.random(),
            randomId = '' + baseId + randomStr.substring(2, maxRandomSize);
        this.lastUniqueId = baseId;
        return randomId;
    },
    zeroPad(num: number, places: number) {
        return String(num).padStart(places, '0')
    },
    getDateString(unix_timestamp: any) {
      const date = new Date(unix_timestamp * 1000);
      return ''   + $$.zeroPad(date.getHours(),2)
            + ':' + $$.zeroPad(date.getMinutes(),2)
            + ':' + $$.zeroPad(date.getSeconds(),2);
    },
    getUTCDateString(unix_timestamp: any) {
      const date = new Date(unix_timestamp * 1000);
      const parts = date.toUTCString().split(' ')
      return `${parts[4]}`
    },
    getToday() {
        return new Date((new Date()).valueOf());
    },
    getYesterday() : Date {
        return new Date((new Date()).valueOf() - 1000*60*60*24);
    },
    datestrAsDate(value: string) {
        return Date.parse(value);
    },
    datestrAsInt(value: string) {
        const date = new Date(value);
        const result = date.getTime();
        return result;
    },
    isDefined(value: any): boolean {
        return typeof(value) !== 'undefined' && value !== null
    },
    isArray: function(value: any): boolean {
        return Array.isArray(value)
    },
    isString : function (value: any): boolean {
        return (typeof value === 'string' && value.length > 0)
    },
    isFunction : function (value: any): boolean {
        return $$.isDefined(value) && (typeof value === 'function')
    },
    isObject : function (value: any): boolean {
        return $$.isDefined(value) && (typeof value === 'object')
    },
    isArrayBuffer(value: any): boolean {
        return value && value instanceof ArrayBuffer && value.byteLength !== undefined;
    },
    isBlob(value: any): boolean {
        if ($$.isDefined(value) && $$.isString(value)) {
            if ( value[0] === 'b' && value[1] === "'") {
                return true
            }
        }
        return false
    },
    isIncludes: function(arr: any, value: any): boolean {
      return $$.isArray(arr) ? arr.includes(value) : false
    },
    replaceNonAlphaChars(value: string, replacer= '-'): string {
        return value.replace(/\W/g, replacer)
    },
    toLower (value: any): string {
        return $$.isString(value) ? value.toLowerCase() : '';
    },
    toUpper (value: any): string {
        return $$.isString(value) ? value.toUpperCase() : '';
    },
    getDottedValue: function(key: string, dict: any): any {
        const idx = key.indexOf('.');
        if (idx === -1) {
            return dict[key];
        }
        const a = key.substring(0, idx),
              b = key.substring(idx + 1);
        return this.getDotted_value(b, dict[a])
    },
    hasKey: function (obj: any, key: string) {
      return key in obj;
    },

    safeGet: function (obj: Record<string, any>, params: string[], defaultValue: null | any = null) :any {
      if (!$$.isObject(obj)) {
        return this.isDefined(defaultValue) ? defaultValue : obj;
      }
      const name = params.shift();

      if (name && params.length > 0 && $$.hasKey(obj, name)) {
        return $$.safeGet(obj[name], params, defaultValue);
      }
      return name && $$.hasKey(obj, name) ? obj[name] : defaultValue;
    },
    safeSet(obj: Record<string, any>, params: string[], value: any): any {
        const name = params.shift(),
              size = params.length;

        if (name && ! $$.isDefined(obj[name])) {
            obj[name] = size > 0 ? {} : value
        }
        return name && size > 0 ? $$.safeSet(obj[name], params, value) : obj;
    },
    checkedInsert(arr1: any[], arr2: any[]) {
        // insert all items from arr1 to arr2 checking if them are't in arr1
        if ($$.isDefined(arr2)) {
            if ($$.isArray(arr1)) {
                arr2 = $$.isArray(arr2) ? arr2 : [arr2]
                arr2.forEach(item => {
                    if (!$$.isIncludes(arr1, item)) {
                        arr1.push(item)
                    }
                })
            }
        }
        return arr1
    },
    firstChars(val: string, count: number, safed = false): string {
        val =  safed ? val : '' + val;
        return val.slice(0, count)
    },

    lastChars(val: string, count: number, safed = false): string {
        val =  safed ? val : '' + val;
        return val.slice(-count)
    },

    skipLastChars(val: string, count: number, safed = false): string {
        val =  safed ? val : '' + val;
        if (val.length > 2) {
          return val.slice(0, -count)
        }
        return val;
    },

    raiseEvent(eventName: string, element: Document | undefined=undefined, params: any) {
        element = element || window.document;
        const eventDict = { bubbles: false, cancelable: false, 'data': params};
        const nativeEvent = new MessageEvent(eventName,  eventDict);
        element.dispatchEvent(nativeEvent);
    },

    mouseEvent(eventName: string, element: any, x: number, y: number) {
      const event = new MouseEvent(eventName, {
            'view': window,
            'bubbles': true,
            'cancelable': true,
            'clientX': x || 0,
            'clientY': y || 0
        });
        element.dispatchEvent(event);
    },
    addEventListener(element: any, event: any, handler: any) {
      element = element || window;
      if (element.attachEvent)      { return element.attachEvent('on' + event, handler) }
      if (element.addEventListener) { return element.addEventListener(event, handler, true) }
      element['on' + event] = handler;
    },
    removeEventListener(element: any, event: any, handler: any) {
      element = element || window;
      if (element.detachEvent)         { return element.detachEvent('on' + event, handler) }
      if (element.removeEventListener) { return element.removeEventListener(event, handler, true) }
      element['on' + event] = null;
    },
    trackMoving(element: any,
                moveMethod?: (startX: number, startY: number, currentX: number, currentY: number) => void,
                resultEventName?: string) {
      let startX = 0;
      let startY = 0;

      const mousemove = (e: any) => {
        const currentX = e.pageX;
        const currentY = e.pageY;
        if (moveMethod) {
          moveMethod(startX, startY, currentX, currentY);
        }
        if (resultEventName) {
          $$.raiseEvent(resultEventName, undefined, { startX, startY, currentX, currentY })
        }
      }
      const mouseup = (e: any) => {
          $$.removeEventListener(undefined, 'mousemove', mousemove);
          $$.removeEventListener(undefined, 'mouseup', mouseup);
      }
      const mousedown = (e: any)=> {
          startX = e.pageX;
          startY = e.pageY;
          $$.addEventListener(undefined, 'mousemove', mousemove);
          $$.addEventListener(undefined, 'mouseup', mouseup);
      }

      $$.addEventListener(element, 'mousedown', mousedown);
    },

    isLikeAsJsonStr(str: string) {
        const   f = this.firstChar(str),
                l = this.lastChar(str);
        return f === '{' && l === '}' || f === '[' && l === ']'
    },
    isLikeAsUrlStr(str: string) {
        if (this.isString(str)) {
            return this.firstChars(str, 4, 'checked') === 'www.'
                || this.firstChars(str, 7, 'checked') === 'http://'
                || this.firstChars(str, 8, 'checked') === 'https://'
        }
        return false
    }
};
