import { RouteRecordRaw } from 'vue-router';
import TableExample01 from 'src/app/Example/pages/TableExample01.vue';
import TableExample02 from 'src/app/Example/pages/TableExample02.vue';
import {isFirstSession} from 'src/app/DbBrowser/myutils';


const routes: RouteRecordRaw[] = [
  {
    path: '/testpage',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('src/app/Example/pages/IndexPage.vue') }],
  },
    {
    path: '/xtable',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => TableExample01}],
  },
  {
    path: '/ztable',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => TableExample02}],
  },
  {
    path: '/page1',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('src/app/Example/pages/MyPage1.vue') }],
  },
  {
    path: '/page2',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('src/app/Example/pages/MyPage2.vue') }],
  },
  // {
  //   path: '/page3',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [{ path: '', component: () => import('pages/ChangePassword.vue') }],
  // },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [isFirstSession()],
    // children: [localStorage.userInfo ? { path: '', component: () => import('../app/DbBrowser/pages/DbBrowserMain.vue') } : { path: '', component: () => import('pages/FirstPage.vue') }],
    // children: [{ path: '', component: () => import('src/app/DbBrowser/pages/DbBrowserMain.vue') }],
  },
    {
    path: '/start',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('src/app/DbBrowser/pages/DbBrowserMain.vue') }],
  },
  {
    path: '/weblogin',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('src/app/DbBrowser/pages/WebLogin.vue') }],
  },
  {
    path: '/reguser',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('src/app/DbBrowser/pages/RegUser.vue') }],
  },

  // {
  //   path: '/iconsFA',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [{ path: '', component: () => import('src/app/info/pages/IconsFontAwesome.vue') }],
  // },
  // {
  //   path: '/iconsFL',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [{ path: '', component: () => import('src/app/info/pages/IconsFlaticon.vue') }],
  // },
  // {
  //   path: '/iconsBT',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [{ path: '', component: () => import('src/app/info/pages/IconsBootstrap.vue') }],
  // },
  // {
  //   path: '/buttons',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [{ path: '', component: () => import('src/app/Example/pages/XButtons.vue') }],
  // },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
