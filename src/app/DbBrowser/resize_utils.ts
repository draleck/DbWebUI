import { $$ } from 'src/system/utils';

export const rizeSplitterHorizontalMoveInfo = (splitter: HTMLElement | null, eventName: string) => {
  if (splitter) {
    const parent = splitter.parentElement;
    const parentStyle = parent?.style;
    if (parentStyle !== undefined) {
      parentStyle['overflow'] = 'hidden';
    }
    const rect = splitter.getBoundingClientRect();
    const wrapperVisibleWidth = rect.width;
    $$.raiseEvent(eventName, undefined, { wrapperVisibleWidth });
  }
}

export const rizeSplitterVerticalMoveInfo = (splitter: HTMLElement | null, eventName: string) => {
  if (splitter) {
    const parent = splitter.parentElement;
    const parentStyle = parent?.style;
    if (parentStyle !== undefined) {
      parentStyle['overflow'] = 'hidden';
    }
    const rect = splitter.getBoundingClientRect();
    const wrapperVisibleHeight = rect.height;
    $$.raiseEvent(eventName, undefined, { wrapperVisibleHeight });
  }
}
