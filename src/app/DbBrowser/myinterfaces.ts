export interface IWsIdData {
  [uniqueId: string]: string
}


export interface IDbConnectors {
  [keyname: string]: {
    typeId: number,
    isEdit: boolean,
    urlPrefix: string,
    icon: string
    default: {
      hostName: string,
      portNumber: number | null,
    }
    flags: {
      readOnly: boolean,
      showSystem: boolean
    }
  }
}

export interface IConnector {
  name: string,
  type: string,
  id: number
}

export interface IWsConnStatus {
  [keyname: string]: boolean | null;
};

export interface ICatalogItem {
  schema: string,
  system: boolean,
  name: string,
  path: string,
  sql: string
}
export interface ICurrentConnection {
  catalog: ICatalogItem
  connector: IConnector
}

export interface IWorkLogDataDownloadProgress {
  timeStamp: number,
  progressStatus: number,
  bytes: number
};
export interface IWorkLogDataDownload {
  [startTimeStamp: string]: {
    action: string,
    data: {
      url: string,
      size?: number,
      progress?: IWorkLogDataDownloadProgress
    }
  }
}
export interface IWorkLogData {
  download: IWorkLogDataDownload
}

export interface IColumns {
  name: string,
  label: string,
  field: string,
  align: 'left' | 'right' | 'center' | undefined
}

export interface IConnOptions {
  read_only: boolean,
  [param: string]: any;
}

export interface IRowConnConnector {
  connName?: string,
  connId?: number | null,
  connTypeId?: number | null,
  connTypeName?: string,
  url?: string,
  options: IConnOptions
}
export interface IConnConnector extends IRowConnConnector {
  hostName: string | null,
  portNumber: number | null,
  userName?: string,
  password?: string,
  dbName?: string,
  comment?: string,
}

export interface IResponseMetaModel {
  name: string,
  key?: string,
  PK?: string,
  type: string,
  constraints: object[]
}
export interface IResponseForTable {
  data: [],
  meta: {
    query: string,
    schema: string,
    table: string,
    limit: number,
    offset: number,
    model: IResponseMetaModel[]
  }

}
