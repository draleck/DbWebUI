export interface IPopupWindow {
  title:      string,
  positionX:  number,
  positionY:  number,
  height?:    number,
  width?:     number,
}

export interface IPopupWindows {
  [keyname: string]: IPopupWindow
};
