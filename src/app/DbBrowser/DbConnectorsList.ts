import { IDbConnectors } from './myinterfaces'

export const dbConnectors: IDbConnectors = {
  unknown: {
    typeId: -1,
    isEdit: false,
    urlPrefix: 'url',
    icon  : './servicesIcons/PostgreSQL.png',
    default: {
      hostName: 'localhost',
      portNumber: 0
    },
    flags: {
      readOnly: true,
      showSystem: true
    }
  },

  Postgres: {
    typeId: 1,
    isEdit: false,
    urlPrefix: 'postgres',
    icon  : './servicesIcons/PostgreSQL.png',
    default: {
      hostName: 'localhost',
      portNumber: 5432
    },
    flags: {
      readOnly: true,
      showSystem: true
    }
  },

  Redis: {
    typeId: 2,
    isEdit: false,
    urlPrefix: 'redis',
    icon  : './servicesIcons/Redis.png',
    default: {
      hostName: 'localhost',
      portNumber: 6379
    },
    flags: {
      readOnly: true,
      showSystem: true
    }
  },
  Zookeeper: {
    typeId: 3,
    isEdit: false,
    urlPrefix: 'url',
    icon  : './servicesIcons/zookeeper_96.png',
    default: {
      hostName: 'localhost',
      portNumber: 2181
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  Kafka: {
    typeId: 4,
    isEdit: false,
    urlPrefix: 'url',
    icon  : './servicesIcons/kafka_w.png',
    default: {
      hostName: 'localhost',
      portNumber: 9092
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  GoogleSheets: {
    typeId: 5,
    isEdit: false,
    urlPrefix: '',
    icon  : './servicesIcons/g_sheets.png',
    default: {
      hostName: '',
      portNumber: -1
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  Rabbitmq: {
    typeId: 6,
    isEdit: false,
    urlPrefix: 'amqp',
    icon  : './servicesIcons/RabbitMQ.png',
    default: {
      hostName: 'localhost',
      portNumber: 5672
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  MySQL: {
    typeId: 7,
    isEdit: false,
    urlPrefix: 'mysql',
    icon  : './servicesIcons/MySQL.png',
    default: {
      hostName: 'localhost',
      portNumber: 3306
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  Cassandra: {
    typeId: 8,
    isEdit: false,
    urlPrefix: 'cassandra',
    icon  : './servicesIcons/Cassandra.png',
    default: {
      hostName: 'localhost',
      portNumber: 9042
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  ClickHouse: {
    typeId: 9,
    isEdit: false,
    urlPrefix: 'clickhouse',
    icon  : './servicesIcons/ClickHouse.png',
    default: {
      hostName: 'localhost',
      portNumber: 8123
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  SQLite: {
    typeId: 10,
    isEdit: false,
    urlPrefix: 'sqlite',
    icon  : './servicesIcons/SQLight.png',
    default: {
      hostName: '..',
      portNumber: null
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  FileExplorer: {
    typeId: 11,
    isEdit: false,
    urlPrefix: 'file',
    icon  : './servicesIcons/catalog.png',
    default: {
      hostName: '..',
      portNumber: null
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  Timescale: {
    typeId: 14,
    isEdit: false,
    urlPrefix: 'postgres',
    icon  : './servicesIcons/TimeScale.png',
    default: {
      hostName: 'localhost',
      portNumber: 5432
    },
    flags: {
      readOnly: true,
      showSystem: true
    }
  },
  FTP: {
    typeId: 12,
    isEdit: false,
    urlPrefix: 'ftp',
    icon  : './servicesIcons/FTP.png',
    default: {
      hostName: 'localhost',
      portNumber: 21
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
  SSH: {
    typeId: 13,
    isEdit: false,
    urlPrefix: 'ssh',
    icon  : './servicesIcons/Ssh.png',default: {
      hostName: 'localhost',
      portNumber: 22
    },
    flags: {
      readOnly: true,
      showSystem: false
    }
  },
}
