
/* The best name of this file should be applied for the future :D

 */

export interface CatalogRecord {
  name :string;
  schema :string;
}

export type TableConstraintType = 'UNIQUE' | 'PK' | 'FK';
export type DatabaseFieldType = 'integer' | 'text' | 'bool' | 'string';

export type TableRecordType = {
    name: string
    type: DatabaseFieldType,
    key: TableConstraintType,
    constraints: Record<TableConstraintType, string>[]
}


export interface RecordInterface {
  name: string;
  label: string;
  field: string;
  key?: TableConstraintType,
  // type: DatabaseFieldType;
  type: string;
  align?: string;
  format?: (val: any) => string;
}
