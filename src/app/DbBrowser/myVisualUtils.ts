
export const makeGradientColors = (array: Array<number>, baseColor = 250) => {
  const maxNumber = Math.max( ...array);
  const colorsArray = [];

    for (let i = 0; i < array.length; i++) {
      const colorBrightness = (maxNumber - array[i])*100/maxNumber;
        colorsArray.push(`hsl(${baseColor}, 100%, ${colorBrightness * 0.6 + 30}%)`);
    }

  return colorsArray;
}
