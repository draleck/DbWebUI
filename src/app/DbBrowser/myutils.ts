import axios, { AxiosHeaders, AxiosResponse } from 'axios';
import { AxiosRequestConfig, AxiosRequestHeaders } from 'axios/index';
import { reactive, ref } from 'vue';
import { SERVER_URL } from 'src/local_settings';
import { IConnector, ICurrentConnection, IWorkLogData, IWsConnStatus, ICatalogItem, IResponseForTable, IResponseMetaModel, IWsIdData } from './myinterfaces';
import { RecordInterface } from './interfaces/general';
import { useQuasar } from 'quasar';
// import {Cookies} from "quasar";

export const serverUrl = SERVER_URL;
// const serverUrl = 'http://tetratest.mooo.com:7775';
// const serverUrl = 'http://localhost:7775';

// axios.defaults.xsrfHeaderName = "X-CSRFToken";
// axios.defaults.xsrfCookieName = 'csrftoken';
export const answConnectors  = ref([]);
export const answConnInfo    = ref({'data': []});
export const answFetch       = ref([] as any);
export const answCatalog     = ref([] as any[]);

export const dataReceiveQueue = ref({} as IWsIdData);


class User {
  id:     string;
  name:   string;
  token:  string;
  constructor() {
    this.id     = ''
    this.name   = ''
    this.token  = ''
  }
}

export const WorkLogData:IWorkLogData = reactive({download: {}})

// export const testBinanceWSData: Ref<object[]> = ref([]);
export const testBinanceWSData  = ref();
export const wsConnStatus       = ref({} as IWsConnStatus);
export const WebDBUniqIndex     = '_WebDBUniqIndex';
export const isLogin            = ref(false);
export const currentConnection: ICurrentConnection = reactive({
  catalog: {
    name: '',
    schema: '',
    system: false,
    path:   '',
    sql:    ''
  },

  connector: {
    name: '',
    type: '',
    id:   -1
  }
});

const LOCAL_STORAGE_USER_KEY = 'userInfo';

export const restoreKey = (key: string) => {
  if (window.localStorage.getItem(key)) {
    const val = window.localStorage.getItem(key) || '';
    return JSON.parse(val);
  }
  return undefined;
}

export const DarkSwich = ref(restoreKey('dark_theme') !== false && true);

export const storeKey = (key: string, val: any) => {
  window.localStorage.setItem(key, JSON.stringify(val));
  return val
}
export const restoreUser = () => {
  if (window.localStorage.getItem(LOCAL_STORAGE_USER_KEY)) {
    const userStr = window.localStorage.getItem(LOCAL_STORAGE_USER_KEY) || '';
    return JSON.parse(userStr);
  }
  return undefined;
}

const resetUser = () => {
  window.localStorage.removeItem(LOCAL_STORAGE_USER_KEY);
}

export const storeUser = (userInfo: User): User => {
  window.localStorage.setItem(LOCAL_STORAGE_USER_KEY, JSON.stringify(userInfo));
  return userInfo
}

const set_headers = () => {
  const headers = {} as AxiosRequestHeaders;

  const user = restoreUser();
  // console.log('############## user', user)

  if (user && user.token) {
    headers['Authorization'] = `Bearer ${user.token}`
  }

  headers['Access-Control-Allow-Credentials'] = true;
  headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE';
  headers['Access-Control-Allow-Headers'] = 'Content-Type, Authorization';
  headers['Content-Type'] = 'application/json';
  return headers;
}


// @todo move all Login/logout/user related into a dedicated module
export async function userLogout() {
  console.log('logout fnction');
  // resetUser(); // move to getData
  const resp = await getData('logout');
  isLogin.value = false;
  return resp;
}

export async function getLogin(userName: string, password: string): Promise<User | null> {
  const response = await getData(`login/${userName}/${password}`);
  const data = response.data;
  if (!!data.token) {
    return storeUser(data as User);
  }
  return null;
}

export async function postLogin(username: string, password: string): Promise<User | null> {
  const response = await postData('login', { username, password });
  const data = response.data;
  if (!!data.token) {
    return storeUser(data as User);
  }
  return null;
}

export const getConfig = (currTimeStamp: string) => ({
  headers: set_headers() as AxiosRequestHeaders,
  withCredentials: true,

  onDownloadProgress(progressEvent) {
    WorkLogData.download[currTimeStamp].data.size = progressEvent.total;
    WorkLogData.download[currTimeStamp].data.progress = ({
      timeStamp: new Date().getDate(),
      // progressStatus: (progressEvent.progress) ? Math.round(progressEvent.progress * 100) : -1,
      progressStatus: (progressEvent.progress) ? progressEvent.progress : -1,
      bytes: progressEvent.loaded
    })
    // console.log(`>>>>>>> DOWNLOAD ${(progressEvent.progress)
    //     ? Math.round(progressEvent.progress * 100)
    //     : 'error'}% (${progressEvent.loaded}/${progressEvent.total} bytes)`);
  },
  onUploadProgress(progressEvent) {
    console.log('<<<<<<< UPLOAD ', progressEvent);
  },
} as AxiosRequestConfig);

async function getData(route: string) {
  const currTimeStamp = String(new Date().getTime());
  const url = `${serverUrl}/${route}`;
  WorkLogData['download'][currTimeStamp] = {
    action: 'getData',
    data: {
      url: route,
    }
  };
  const header = getConfig(currTimeStamp);
  if (route === 'logout') resetUser();
  const response = await axios.get(url, header);
  return response;
}

export async function postData(route: string, data: object) {
  const url = `${serverUrl}/${route}`;
  const currTimeStamp = String(new Date().getTime());
  WorkLogData['download'][currTimeStamp] = {
    action: 'postData',
    data: {
      url: route
    }
  };


  // WorkLogData.download[currTimeStamp].action = 'postData';
  // WorkLogData.download[currTimeStamp].data['url'] = url;
  const response = await axios.post(url, data, getConfig(currTimeStamp));
  return response;
}

export async function getConnectors() {
  const response = {data: []};
  await getData('connectors/')
    .then(val => response.data = val.data['data'])
    .catch((err) => {
    if (err['response']&& (err['response']['status'] === 403)) userLogout();
  });
  // console.log('!!!!!! data = ', response.data);
  return response.data;
}

export async function getConnectorInfo(connector: IConnector) {
  const { data } = await getData(`connector_info/?connector=${connector.name}&connector_id=${connector.id}`);
  return data;
}

export async function getCatalog(connector: IConnector, directory: string | undefined, refresh = false) {
  const url = `catalog/?connector=${connector.name}&connector_id=${connector.id}${directory ? '&directory=' + encodeURIComponent(directory) : ''}&refresh=${refresh}`;
  const { data } = await getData(url);
  // console.log('getCatalog data ', data);


  // return Object.keys(data['catalog']).length === 0 ? [] : data['catalog'];
  return data;
}

const convertRows = (arrRows: string[], data: IResponseMetaModel[] ) => {
  const row = {} as {[key: string]: string};
  for (let i = 0; i < arrRows.length; i++) {
    // row[String(data['meta']['field_names'][i])] = arrRows[i]
    row[String(data[i]['name'])] = arrRows[i]
  }
  return row;
}

export const getTableModel = (val: IResponseMetaModel[]) => {
  if (!val) return [];
  const result: RecordInterface[] = val.map((item: IResponseMetaModel) =>

        item.type === 'ARRAY'
      ? ({
        name: item.name,
        label: item.name + '[arr]',
        field: item.name,
        type: item.type,
        align: 'left',
        format: (val: any) => JSON.stringify(val),
      })
      : ({
        name: item.name,
        label: item.name,
        field: item.name,
        type: item.type,
        align: 'left',
      })

    );
  return result;
}



export const porocessingSqlAnswer = (data: IResponseForTable) => {
  // const result = (data['data'] === undefined || Object.keys(data['data']).length === 0)
  // console.log('!!! porocessingSqlAnswer <data> ', data)
  const result = (data['data'] === undefined)
  ? []
  : data['data'].map((item: string[], indx: number) =>
      ({ [WebDBUniqIndex]: indx, ...convertRows(item, data['meta']['model']) }));
  return { 'rows': result, 'meta': data['meta'] || []} ;
}

export async function postSQL(sqldata: object, route = 'execute/') {
  const { data } = await postData(route, sqldata);

return data;
};

export async function getProcesses(connectorId: string) {
  const { data } = await postData('processes/', {connector_id: connectorId});
  return porocessingSqlAnswer(data);
}

export async function getFetch(connector: IConnector, catalog: ICatalogItem, directory: string | undefined, refresh = false) {

  const url = 'fetch/'
    + `?connector=${connector.name}`
    + `&connector_id=${connector.id}`
    + `&schema=${encodeURIComponent(catalog.schema)}`
    + `&table=${encodeURIComponent(catalog.name)}`
    + `${directory ? '&directory=' + encodeURIComponent(directory) : ''}`
    + `&refresh=${refresh}`;
  const { data } = await getData(url);


  // const result = Object.keys(data['data']).length === 0
  //   ? []
  //   // : data['data'].map((item: object, indx: number) => ({ [WebDBUniqIndex]: indx, ...item }));
  //   : data['data'].map((item: string[], indx: number) =>
  //       ({ [WebDBUniqIndex]: indx, ...convertRows(item, data['meta']['model']) }));
  //   return { 'rows': result, 'meta': data['meta'] } ;

  // return porocessingSqlAnswer(data);

  return data;
}

export async function getScience(currConn: ICurrentConnection, directoryName = '',
                                 mode = 'table', action = 'analysis') {

  const directory = encodeURIComponent(directoryName);
  const url = 'science/'
    + `?connector=${currConn.connector.name}`
    + `&table=${currConn.catalog.name}`
    + `&connector_id=${currConn.connector.id}`
    + `&directory=${directory}`
    + `&mode=${mode}`
    + `&action=${action}`;

  const { data } = await getData(url);
  if (mode === 'catalog') {
    const result = Object.keys(data['data']).length === 0
      ? []
      : data['data'].map((item: string[], indx: number) => (
        { [WebDBUniqIndex]: indx, ...convertRows(item, data['meta']['model']) }
      ));
    return { 'rows': result, 'meta': data['meta'] };
  }
  else {
    const rows = { [WebDBUniqIndex]: 0, [currConn.catalog.name]: JSON.stringify(data) }
    return { 'rows': [rows], 'meta': [] };
  }
}



export async function getExtraInfo(currConn: ICurrentConnection, catalogName = '',
                                 mode = 'table', action = 'default') {
  const catalog = encodeURIComponent(catalogName);
  const url = 'extrainfo/'
    + `?connector=${currConn.connector.name}`
    + `&schema=${currConn.catalog.schema}`
    + `&table=${currConn.catalog.name}`
    + `&connector_id=${currConn.connector.id}`
    + `&catalog=${catalog}`
    + `&mode=${mode}`
    + `&action=${action}`

  const { data } = await getData(url);

  if (mode === 'catalog') {
    const result = Object.keys(data['data']).length === 0
      ? []
      : data['data'].map((item: string[], indx: number) => (
        { [WebDBUniqIndex]: indx, ...convertRows(item, data['meta']['model']) }
      ));
    return { 'rows': result, 'meta': data['meta'] };
  }
  else {
    const rows = { [WebDBUniqIndex]: 0, [currConn.catalog.name]: JSON.stringify(data) }
    return { 'rows': [rows], 'meta': [] };
  }

}

export function isJSON(json: string) {
    try {
      const obj = JSON.parse(json)
      if (obj && typeof obj === 'object' && obj !== null) {
        return true
      }
    } catch (err) {}
    return false
}

export const tableModel = ref([] as RecordInterface[]);


export const useNotify = () => {
  const $q = useQuasar();
  const showNotify = (message: string, type: string | undefined = undefined, timeOut = 3000) => {
    $q.notify({
          position: 'top',
          timeout: timeOut,
          type: type,
          closeBtn: timeOut === 0 || timeOut > 3000 && 'Close!',
          message: message
        });
  }
  return { showNotify };
}

export async function stopProcess(rowData: object) {
  console.log('Kill proc FUNC postData ', rowData);
  const result = await postData('postgres/', rowData);
  return result;
}

export async function dbSourcesResponse(data: object) {
  // const url = `${serverUrl}/connector/`;
  console.log('DATA DATA DATA DATA DATA: ', data);
  // const response = await axios.post(url, data, getConfig(currTimeStamp));
  const response = await postData('connector/', data)
  return response;
}


export const dbSourcesParams = {
  postgres: {
    id: null,
    connectorName: '',
    driver: 'Postgres',
    dlgWindowTitle: 'Postgres Connection',
    isHostNameShow: true,
    isHostShow: true,
    isPortShow: true,
    isUserShow: true,
    isPasswordShow: true,
    isDatabaseShow: true,
    isUrlShow: true,
    urlTitleName: 'URL: ',
    urlValue: 'postgresql://127.0.0.1:5432/0',
    isReadonlyCheckboxShow: true,
    isShowSystemObjectsCheckboxShow: true,
    isCommentShow: true,
  },
  redis: {
    id: null,
    connectorName: '',
    driver: 'Redis',
    dlgWindowTitle: 'Redis Connection',
    isHostNameShow: true,
    isHostShow: true,
    isPortShow: true,
    isUserShow: true,
    isPasswordShow: true,
    isDatabaseShow: true,
    isUrlShow: true,
    urlTitleName: 'URL: ',
    urlValue: 'redis://127.0.0.1:6379/0',
    isReadonlyCheckboxShow: true,
    isShowSystemObjectsCheckboxShow: false,
    isCommentShow: true,
  },
  csv: {
    id: undefined,
    driver: 'CSV',
    dlgWindowTitle: 'CSV Connection',
    isHostNameShow: true,
    isHostShow: false,
    isPortShow: false,
    isUserShow: false,
    isPasswordShow: false,
    isDatabaseShow: false,
    isUrlShow: true,
    urlTitleName: 'Folder: ',
    isReadonlyCheckboxShow: false,
    isShowSystemObjectsCheckboxShow: false,
    isCommentShow: true,
  }
}

export function parseConnectorString(string: any) {
  console.log('Nastya parse string ', string)
  const urlRegex = /^(?<protocol>[^:]+):\/\/(?:(?<username>[^:@]+):)?((?<password>[^:@]+)@)?(?<host>[^:\/]+):(?<port>\w+)\/(?<database>.+)?$/;

  const match = string.match(urlRegex);

  if (!match) {
    throw new Error('Invalid URL format');
  }

  const protocol = match.groups.protocol || '';
  const username = match.groups.username || '';
  const password = match.groups.password || '';
  const host = match.groups.host || '';
  const port = match.groups.port || '';
  const database = match.groups.database || '';

  return {
    protocol,
    username,
    password,
    host,
    port,
    database,
  };
}

export const isFirstSession = () => {
  if (restoreKey('firstSession') !== undefined) {
    return {path: '', component: () => import('../DbBrowser/pages/DbBrowserMain.vue')}
  }
  storeKey('firstSession', 'false')
  return {path: '', component: () => import('pages/FirstPage.vue')}
}

export function formatUnixTimestamp(unixTimestamp: number | string) {
  const date = new Date(Number(unixTimestamp));
  const day = String(date.getDate()).padStart(2, '0');
  const month = String(date.getMonth() + 1).padStart(2, '0'); // Месяцы начинаются с 0
  const year = date.getFullYear();
  const hours = String(date.getHours()).padStart(2, '0');
  const minutes = String(date.getMinutes()).padStart(2, '0');
  const seconds = String(date.getSeconds()).padStart(2, '0');
  // return `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
  return `${hours}:${minutes}:${seconds}`;
}

