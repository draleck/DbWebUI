export enum ControlVisualStatus {
  Normal = '',
  Hover = 'hover',
  Selected = 'selected',
  Marked = 'marked',
  HoverSelected = 'hover-selected',
  HoverMarked = 'hover-marked'
}

export enum ClickMode {
  click,
  dblClick,
  rightClick
}


export const clickModeAsString = (mode: number) : string => {
  return {
    0: 'click',
    1: 'dblClick',
    2: 'rightClick'
  }[mode] as string;
}
