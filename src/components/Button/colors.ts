export type ButtonColor
    = 'btn-primary'
    | 'btn-secondary'
    | 'btn-warning'
    | 'btn-success'
    | 'btn-danger'
    | 'btn-link'
    | 'btn-info'
    | 'btn-light';

export default ButtonColor;