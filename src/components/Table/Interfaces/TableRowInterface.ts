import { TableColumnInterface } from './TableColumnInterface';
import { TableHeaderColumnInterface } from './TableColumnInterface';


export interface TableRowInterface {
  columns: TableColumnInterface[];
  readonly index?: number;
}

export interface TableHeaderRowInterface {
  columns: TableHeaderColumnInterface[];
  readonly index?: number;
}
