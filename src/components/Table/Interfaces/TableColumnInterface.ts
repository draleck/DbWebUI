
export interface TableColumnInterface {
  name: string;
  value: string;
  hash(): string;
}

export interface TableHeaderColumnInterface {
  name: string;
  hash(): string;
}

export interface TableColumnModel {
  name: string;
  type: string | object | null;
  isActual?: boolean;
}
