import { $$ } from 'src/system/utils';
import { TableColumnInterface, TableColumnModel } from '../Interfaces/TableColumnInterface';
import { TableHeaderColumnInterface } from '../Interfaces/TableColumnInterface';

// Dynamic components
// @see https://medium.com/scrumpy/dynamic-component-templates-with-vue-js-d9236ab183bb
// @see https://rangle.io/blog/how-to-create-data-driven-user-interfaces-in-vue

export class TableColumnType implements TableColumnInterface {
  name: string;
  value: string;

  constructor(name: string, value: string) {
    this.name = name;
    this.value = value;
  }

  hash() { return $$.replaceNonAlphaChars(this.name) }
}

export class TableColumnModelType  implements TableColumnModel {
  name: string;
  title?: string | null;
  type: string | object | null;
  isActual?: boolean;
  params?: object;

  constructor(name: string, type: string | object | null = null, title: string | null = null, params: NonNullable<unknown> = {}) {
    this.name = name;
    this.title = $$.isDefined(title) ? title : this.name;
    this.type = type;
    this.params = params;
    this.isActual = true;
  }

  hash() { return $$.replaceNonAlphaChars(this.name) }
}

export class TableHeaderColumnType extends TableColumnModelType implements TableHeaderColumnInterface {
}
