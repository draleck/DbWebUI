export const TABLE_RESIZE = 'TABLE-RESIZE';
export const TABLE_RESIZE_X = 'TABLE-RESIZE-X';
export const TABLE_RESIZE_Y = 'TABLE-RESIZE-Y';
export const COLUMN_RESIZE = 'COLUMN-RESIZE';

export type MouseEvent = {
  pageX: number,
  pageY: number
};
