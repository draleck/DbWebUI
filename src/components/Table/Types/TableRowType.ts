import { $$ } from 'src/system/utils';

import { TableColumnInterface }       from '../Interfaces/TableColumnInterface';
import { TableHeaderColumnInterface } from '../Interfaces/TableColumnInterface';
import { TableRowInterface }          from '../Interfaces/TableRowInterface';
import { TableHeaderRowInterface }    from '../Interfaces/TableRowInterface';

import { TableHeaderColumnType }      from './TableColumnType';

export class TableRowType implements TableRowInterface {
  columns: TableColumnInterface[];

  constructor(columns: TableColumnInterface[]) {
    this.columns = columns;
  }
}


export class TableHeaderRowType implements TableHeaderRowInterface {
  columns: TableHeaderColumnInterface[];

  // constructor() {
  //       this.columns = [];
  // }

  // constructor(columns: TableColumnInterface[]) {
  //       this.columns = [];
  //       columns.forEach(column => {
  //           this.columns.push(new TableHeaderColumnType(column.name))
  //    })
  // }
  //
    constructor(columns: TableHeaderColumnType[]) {
        this.columns = columns;
        // columns.forEach(column => {
        //     this.columns.push(new TableHeaderColumnType(column.name))
     // })
  }

}
