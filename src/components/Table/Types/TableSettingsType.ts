import { $$ } from 'system/utils';

export class TablePaginatorType {
  applied: boolean;
  pageValues: any[];


  public constructor(record: Partial<TablePaginatorType> = {}) {
    this.applied = true;
    this.pageValues = [
        {'title': 5, 'value': 5},
        {'title': 7, 'value': 7},
        {'title': 10, 'value': 10},
        {'title': 11, 'value': 11},
        {'title': 13, 'value': 13},
        {'title': 20, 'value': 20},
        {'title': 30, 'value': 30},
        {'title': 33, 'value': 33},
        {'title': 100, 'value': 100},
        {'title': 'all', 'value': 100000},
    ]
    Object.assign(this, record);
 }
}

export class TableColumnResizeSettingsType {
  lastColumnResizable: boolean;
  tableHeightMode: string;

  constructor() {
    this.lastColumnResizable = false;
    this.tableHeightMode = '';
  }
}


export class SpecialColumnsType {
    enumerator: boolean;
    star: boolean;

    constructor() {
      this.enumerator = false;
      this.star = false;
    }
}

export const defaultComponentClasses = {
      'table':                'view-table',
      'table-head-row':       'view-table-head-row',
      'table-row':            'view-table-row',
      'table-column':         'view-table-column',
      'table-row-select-column': 'view-table-row-select-column',
      'control.string':       'view-table-control-string',

      'table-bottom-row':     'view-table-bottom-row',
      'table-scroll-column':  'view-table-scroll-column',
      'table-searcher':       'view-table-searcher',

      'icon-toggle-button' :  'icon-toggle-button',

      'ExampleTable':         'example-table',
      'TableHeadColumn':      'view-table-header-column',
    };

export const defaultComponentStyles = {
      'Table': { color: 'black' }
    };

export class TableSettingsType {
  tableHeight: number;
  tableWidth: number;
  columnResizable: boolean;
  columnFixedSize: boolean;
  columnResize: TableColumnResizeSettingsType;

  rowSelector: boolean;
  rowEnumerator: boolean;
  rowEnumeratorName: string;

  specialColumn: SpecialColumnsType;    // not used yet

  componentClasses: Record<string, string>;
  componentStyles: Record<string, Record<string, string>>;
  paginator: TablePaginatorType | null;
  searchApplied: boolean;
  virtualScroll: boolean;

  public constructor(record: Partial<TableSettingsType> = {},
                       classes: Record<string, string> = {},
                       styles: Record<string, any> = {}) {
    this.searchApplied = true;
    this.columnResizable = true;
    this.columnFixedSize = true;

    this.columnResize = {
        lastColumnResizable: false,
        tableHeightMode: 'table'
    } as TableColumnResizeSettingsType;

    this.specialColumn = {      // not used yet
        enumerator: true,
        star: false
    } as SpecialColumnsType;

    this.tableHeight = 0;
    this.tableWidth = 0;
    this.rowSelector = false;
    this.rowEnumerator = false;
    this.rowEnumeratorName = '#';

    this.componentClasses = classes || defaultComponentClasses;

    this.componentStyles = styles || defaultComponentStyles;

    this.paginator = new TablePaginatorType();
    this.virtualScroll = true;

    Object.assign(this, record);
   }
}


export const getSettingsForDebug = () => {
  const sett = new TableSettingsType();
  return sett;
}

export const getSettingsForTableView = () => {
  const sett = new TableSettingsType();
  sett.paginator = null;
  sett.searchApplied = false;
  return sett;
}
