import StringControl from 'components/Controls/StringControl.vue';
import NumberControl from 'components/Controls/NumberControl.vue';
import BooleanControl from 'components/Controls/BooleanControl.vue';
import {BarXYPercentage} from 'components/Table/Implementation/dataScrollBar';
import { TableStringField } from 'components/Table/Fields/TableStringField';
import { TableColumnModelType } from 'components/Table/Types/TableColumnType';
// import {SearchViewModes} from 'components/Table/Types/TableViewType';

const DEFAULT_PAGE_SIZE = 10000;
export const DEFAULT_WINDOW_ROWS = 20;
const DEFAULT_ROW_HEIGHT = 20;
const DEFAULT_WHEEL_RATE = 4;
const DEFAULT_MINSCROLLBARSIZE = 20;


export type YCoordinate = number;
export type XCoordinate = number;

export interface RecordExtraInfo {
        _idx_?: number;         // global record index based on 'Whole Data'
        _num_?: number;         // global record number based on 'Whole Data'
       _info_?: object;         // for the future
       _deleted_?: boolean;
       _hidden_?: boolean;
}

export const isSpecialColumn = (columnName: string) => [
  '_idx_', '_num_', '_info_', '_deleted_', '_hidden_'].includes(columnName);

export type DataRowType = Record<string, any> | RecordExtraInfo;
export type InputDataType = DataRowType[];

export interface RenderRecordSystemInfoInterface {
    rowNumber: number;      // who knows what this
    rowIndex: number;      // who knows what this
    indexData: number;
    indexPage: number;
    indexActual: number;
    even: boolean;
    odd: boolean;
    empty: boolean;
    first: boolean;
    last: boolean;
    inner: boolean;
    pagesize: number;
    page: number;
    pages: number;
}

export class RenderRecordSystemInfo implements RenderRecordSystemInfoInterface {
    rowIndex: number;
    rowNumber: number;
    indexData: number;
    indexPage: number;
    indexActual: number;
    even: boolean;
    odd: boolean;
    empty: boolean;
    first: boolean;
    last: boolean;
    inner: boolean;
    pagesize: number;
    page: number;
    pages: number;

    public constructor(record: Partial<RenderRecordSystemInfo> = {}) {
        this.rowIndex = 0;
        this.rowNumber = 0;
        this.indexData = 0;
        this.indexPage = 0;
        this.indexActual = 0;
        this.even = false;
        this.odd = false;
        this.empty = false;
        this.first = false;
        this.last = false;
        this.inner = false;
        this.pagesize = 0;
        this.page = 1;
        this.pages = 0;
        Object.assign(this, record);
   }
}

export interface RenderRecordInterface {
    SYS: RenderRecordSystemInfo;
    CONTEXT: object;
    actualNumberOfColumns: number;
    settings: object;
    model: object;
    addinfo: object;
    key: string;
    data: DataRowType;   // row itself
}

export class RenderRecord implements RenderRecordInterface {
    SYS: RenderRecordSystemInfo;
    CONTEXT: object;
    actualNumberOfColumns: number;
    settings: object;
    model: object;
    addinfo: object;
    key: string;
    data: DataRowType;   // row itself

    public constructor(record: Partial<RenderRecord> = {}) {
        this.SYS = new RenderRecordSystemInfo();
        this.CONTEXT = {};
        this.actualNumberOfColumns = 0;
        this.settings = {};
        this.model = {};
        this.addinfo = {};
        this.data = {};
        this.key = '';
        Object.assign(this, record);
   }
}

export class RenderColumn {
    value: any;
    params: any;
    SYS: RenderRecordSystemInfo;
    actualNumberOfColumns: number;
    model: TableColumnModelType;

    public constructor(record: Partial<RenderColumn> = {}) {
        this.SYS = new RenderRecordSystemInfo();
        this.value = '';
        this.params = {};
        this.model = new TableColumnModelType('');
        this.actualNumberOfColumns = 0;
        Object.assign(this, record);
   }
}

// for REMOVING...
export interface XYPercentage {
    x?: number;
    y?: number;
}

export class ScrollInfoType {
    minScrollBarSize: number;
    currentScrollBarSize: number;
    indexY: number;           // additional shift of the first Line
    indexX: number;
    wheelDelta: number;
    wheelRate: number;
    auto: boolean;
    scrollBottom: boolean;     // scroll LAST lines if them less then number of pages

    // reset() {
    //   console.log('############# RESET = PLEASE APPLY ')
    // }

    update(record: Partial<ScrollInfoType> = {}) {
        Object.assign(this, record);
    }

    constructor(record: Partial<ScrollInfoType> = {}) {
        this.minScrollBarSize = DEFAULT_MINSCROLLBARSIZE;
        this.currentScrollBarSize = this.minScrollBarSize;
        this.indexY = 0;
        this.indexX = 0;
        this.wheelRate = DEFAULT_WHEEL_RATE;
        this.wheelDelta = 0;
        this.auto = false;
        this.scrollBottom = false;
        Object.assign(this, record);
    }
}


export enum SearchViewModes {
  Any = 'any',
  Start = 'start'
}


export class SysType {
    lastAction: string;

    constructor() {
        this.lastAction = ''
    }
}

// export class SearchViewType {
export class SearcherType {
    fields: string[];
    types: string [];
    instant: boolean;
    mode: SearchViewModes;
    caseSensitive: boolean;

    update(record: Partial<SearcherType> = {}) {
        Object.assign(this, record);
    }

    constructor(record: Partial<SearcherType> = {}) {
        this.fields = [];
        this.types = ['integer', 'smallintunsigned', 'varchar', 'string', 'str', 'text', 'timestamptz', 'uuid', 'datetime'];
        this.instant = false;
        this.mode = SearchViewModes.Any;
        this.caseSensitive = true;
        Object.assign(this, record);
    }
}

export class SorterItem {
    direction: 'down' | 'up';
    key: string;
    applied: boolean;

    public constructor(record: Partial<SorterItem> = {}) {
        this.direction = 'down';
        this.key = '';
        this.applied = false;
        Object.assign(this, record);
   }
}

export class SorterType {
    key: string;
    direction: string;
    onceSorter: any;
    applied: SorterItem[];
    sortable: SorterItem[];
    fields: string[];
    types: string[];

    update(record: Partial<SorterType> = {}) {
        Object.assign(this, record);
    }

    public constructor(record: Partial<SorterType> = {}) {
        this.key = '';
        this.direction = 'down';
        this.onceSorter = null;
        this.applied = [];
        this.sortable = [];
        this.fields = ['name'];
        this.types =['integer', 'smallintunsigned', 'varchar', 'string', 'str', 'timestamptz', 'uuid', 'datetime'];
        Object.assign(this, record);
   }
}

export class EmptyRecordType {
}

export type FilterType = string | object;

export interface IPoint {
   x: number;
   y: number
}
export class Point  implements IPoint {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}

export const getDefaultColumnSizeByType = (typeName: string | undefined): number | undefined => {
  return typeName ? {
    'boolean': 3,
    'string': undefined,
    'text': undefined,
    'number': 10,
    'float': 10,
    'int': 8,
    'integer': 8,
    'date': 16,
    'timestamp': 12
  }[typeName] : undefined;
}

export class TableViewType {

    actualCountOfRows: number;            // count of rows after filtering, etc

    scroll: ScrollInfoType;
    scrollShift: number;
    textScale: number;
    scrollPercentage: BarXYPercentage;
    scrollBarSizePercentageY: number;

    // move to a dedicated class
    page: number;               // current page
    pageSize: number;
    pageInfo: object;
    windowNumberRows: number;
    rowHeight: number;

    // windowSize: IPoint;

    actualPageSize: number;
    // windowFirstRow: number;

    sys: SysType;
    sorter: SorterType;
    search: SearcherType;
    emptyRecord: EmptyRecordType;
    filter: FilterType;
    columns: NonNullable<unknown>;

    data: any[];
    filters: FilterType[];
    context: object;
    addinfo: object;

    public initColumns() {
        this.columns = {
            'X_string': StringControl,
            'string': TableStringField,
            'X_number': NumberControl,
            'number': TableStringField,
            'boolean': BooleanControl,
            '_default_': TableStringField
          };
    }

    public constructor(initialValues: Partial<TableViewType> = {}) {
              // this.refreshMarker = 0;
        this.actualCountOfRows = 0;
        this.pageSize = DEFAULT_PAGE_SIZE;
        this.actualPageSize = DEFAULT_PAGE_SIZE
        this.windowNumberRows = DEFAULT_WINDOW_ROWS;
        this.rowHeight = DEFAULT_ROW_HEIGHT;
        // this.windowSize = new Point(-1, -1);
        // this.windowFirstRow = 0;
        this.page = 0;
        // this.currentActiveFirstIndex = 0;
        // this.indexShift = 0;
        this.scrollShift = 0;
        this.textScale = 1;
        this.scrollPercentage = {x:0, y:0} as BarXYPercentage;
        this.scrollBarSizePercentageY = 0;
        this.scroll = new ScrollInfoType();
        this.sys = new SysType();
        this.emptyRecord = new EmptyRecordType();
        this.pageInfo = {};
        this.sorter = new SorterType();
        this.search = new SearcherType();
        this.filter = { action_default: 'start' };
        this.columns = {};

        this.data = [];
        this.filters = [];
        this.context = {};
        this.addinfo = {};
        // this.reset();
        this.initColumns();
        Object.assign(this, initialValues);
   }
}

export class BooleanFieldType {
    value: boolean;
    SYS: RenderRecordSystemInfo;
  constructor(value: boolean, SYS: RenderRecordSystemInfo) {
      this.value = value;
      this.SYS = SYS;
  }
}

export type DataRenderResultInfo = NonNullable<unknown>;
