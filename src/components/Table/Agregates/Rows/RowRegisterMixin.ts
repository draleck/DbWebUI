
import { defineComponent } from 'vue'
import { RenderColumn } from 'components/Table/Types/TableViewType';

export default defineComponent({
    data() {
      return {
        // columnBoxes: {},    // remove ??
        columnWidth: [],
        columnHeight: 0
      }
    },
    methods: {
        registerColumnItem(item: RenderColumn, index: number, isLast: boolean) {
            // console.log("+++ REGISTER COLUMN ITEM [" + index + "]", item)
        },
        registerColumnBox(box: DOMRect, index: number, isLast: boolean) {
            // console.log("+++ REGISTER COLUMN BOX [" + index + "]", box)
            // this.columnBoxes[index] = box;
        },
        registerColumn(column: HTMLElement, index: number, isLastColumn: boolean, rowIndex: number, lastRow: boolean) {
            if (this.columnWidth.length <= index) {
                this.columnWidth.push(column.clientWidth);
            }
            if (isLastColumn) {
                this.controller.registerRow(rowIndex, lastRow, this.columnWidth, column.clientHeight);
                this.columnHeight = column.clientHeight;
                // console.log('#### REGISTER COLUMN::[' + index + '] columnHeight:: [' + this.columnHeight + ']')
            }
        }
    }
})

/*

  readonly clientHeight: number;
  readonly clientLeft: number;
  readonly clientTop: number;
  readonly clientWidth: number;

 */
