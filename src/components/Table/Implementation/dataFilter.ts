import { $$ } from 'src/system/utils';
import { FilterType, TableViewType } from 'components/Table/Types/TableViewType';

//FilterType

const addFilter = (view: TableViewType, filter: FilterType) => {
    view.filters.push(filter);
};

// eslint-disable-next-line @typescript-eslint/ban-types
const filterIterator = (view: TableViewType, callback: Function, mode: string) => {
    const filters = view.filters,
        countOfFilters = filters.length;
    let successes = 0;

    for (let i=0; i<filters.length; i+=1) {
        const filter = filters[i];
        let key = filter,
            value = true;

        if ($$.isObject(filter)) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          key = filter['key']
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          value = filter['value'];
        }

        if (callback(key, value, filter)) {
            if (mode === 'first_case_break') {
                return true
            }
            successes += 1
        }
    }
    return successes === countOfFilters
};

const removeAllAutoFilters = (view: TableViewType, filterMode: string) => {
    const temp = [] as FilterType[];
    filterIterator(view, function(key: string, value: any, filter: any) {
            if (filter.filterMode !== filterMode) {
                temp.push(filter);
            }
        }, '');
    view.filters = temp;
};

const filterPerformer = (action: string) => {
    const actions = {
          start   : (value: string, qvalue: any) => { return $$.isString(value) ? value.startsWith(qvalue) : value === qvalue },
          any     : (value: string, qvalue: any) => { return $$.isString(value) ? value.indexOf(qvalue) !== -1 : value === qvalue },
          greater : (value: string, qvalue: any) => { return value < qvalue},
          less    : (value: string, qvalue: any) => { return value > qvalue},
          eq      : (value: string, qvalue: any) => { return value === qvalue},
    };
    return actions[action as keyof typeof actions]
};

const valueExtractor = (key: string, row: any) => {
    let value = $$.getDottedValue(key, row);
    if (value) {
        // wrong data structure fix
        if (Array.isArray(value)) {
            value = value[0]
        }
    }
    return value
};

const filterActionDefault = (value: any, defaultAction: any) => {
    return defaultAction
};

export const filterOk = (view: TableViewType, row: any): boolean => {
  return true;
    // let searchFiltersCount = 0,
    //     searchOk = 0;
    //
    // filterIterator(view, function(filterKey: string, filterValue: any, filter: FilterType) {
    //     const accessor = filter.field ? filter.field : filterKey,
    //         value = filter.value ? filter.value : filterValue;
    //     let   realValue = valueExtractor(accessor, row);
    //     const action = filter.filterAction
    //             || filter.action
    //             || filterActionDefault(realValue, view.filter.action_default);
    //
    //     if (! realValue) {
    //         return false
    //     }
    //
    //     if (filter.filterMode === 'search') {
    //         if (! filter._filter_sensitive_) {
    //             realValue = $$.toLower(realValue)
    //         }
    //         searchFiltersCount += 1;
    //         // Vue.$log.debug('FILTER[' + action + '] [' + value + '] [' + realValue + ']');
    //         searchOk += filterPerformer(action)(realValue, value) ? 1 : 0;
    //     }
    // }, 'first_case_break');
    //
    // return ! (searchOk > 0 || searchFiltersCount === 0)
    //     ? false
    //     : filterIterator(view, (filterKey, filterValue, filter) => {
    //         const accessor = filter.field ? filter.field : filterKey,
    //             value = filter.value ? filter.value : filterValue;
    //
    //     if (filter.filterMode === 'search') {
    //         return true;    // already took into account
    //     }
    //     const realValue = valueExtractor(accessor, row),
    //         action = filter.filterAction || filter.action || view.filter.action_default;
    //
    //     if (! realValue) {
    //         return false
    //     }
    //     return value ? filterPerformer(action)(realValue, value) : false
    // }, '');
}


const removeFilter = (view: TableViewType, filter: FilterType) => {
    // const qkey = $$.isObject(filter) ? filter['key'] : filter,
    const   temp = [] as FilterType[];
    //
    // filterIterator(view, (key, value, filter) => {
    //         if (key !== qkey) {
    //             temp.push(filter);
    //         }
    //     }, '');
    // // todo please use $refs here
    // // if (filter.titleref) {
    // //     $(filter.titleref).html('');
    // // }

    view.filters = temp;
};

const hasSomeFilter = (view: TableViewType): boolean =>  {
    return view.filters.length > 0
};

const isAlreadyAppliedFilter = (view: TableViewType, filterKey: any) => {
  return true;
    // if (! hasSomeFilter(view)) {
    //     return false
    // }
    // return filterIterator(view, function(key, value, filter) {
    //     return filterKey === key;
    // }, 'first_case_break');
};

const toggleFilter = (view: TableViewType, filter: FilterType, value: any) => {
    // const key = $$.isObject(filter) ? filter['key'] : filter;
    console.log('######################## toggleFilter - PLEASE REVIEW')
    console.log('######################## toggleFilter - PLEASE REVIEW')
    console.log('######################## toggleFilter - PLEASE REVIEW')
    console.log('######################## toggleFilter - PLEASE REVIEW')
    // if (isAlreadyAppliedFilter(view, key)) {
    //     return this.removeFilter(view, filter);
    // }
    // else {
    //     return this.addFilter(view, filter)
    // }
};

export default {
    addFilter: addFilter,
    removeFilter: removeFilter,
    removeAllAutoFilters: removeAllAutoFilters,
    filterOk: filterOk
}
