import { $$ } from 'src/system/utils';

import {
    DataRowType,
    RenderRecord,
    RenderRecordInterface,
    DataRenderResultInfo
} from '../Types/TableViewType'

import { TableViewType } from '../Types/TableViewType'
import { dataSorter } from './dataSorter'
import { filterOk } from './dataFilter'
import { wheelDelta } from 'components/Table/Implementation/dataScrollWheel';

export const actualPositive = (value: number, boundValue: number): number => {
    return $$.isDefined(value) && value > 0 ? Math.min(value, boundValue) : boundValue;
};


export const dataIteratorFilter = (
    selectedIdx: number[],    // for keeping lines after filtering
    wholeData: DataRowType[],
    view: TableViewType,
    condition?: (r: DataRowType) => boolean)  => {

  if (selectedIdx.length > 0) {
    // console.log('#################### selectedIdx.length::', selectedIdx.length);
    return;
  }
  condition = condition ? condition : (item: DataRowType) => {return true};
  const maxWholeDataIndex: number = $$.isDefined(wholeData) ? wholeData.length : 0;

  for (let index = 0; index < maxWholeDataIndex; index += 1) {
    const row: DataRowType = wholeData[index];
    if (!row) break
    if (!condition(row)) continue
    if (row._deleted_) continue
    if (row._hidden_) continue

    if (filterOk(view, row)) {
      selectedIdx.push(index);
    }
  }
}

export const dataIterator = (
    wholeData: DataRowType[],
    selectedIdx: Array<number>,
    view: TableViewType,
    rowFormatter: (r: DataRowType, i: number, d: any) => Partial<RenderRecord>, condition: any): [RenderRecordInterface[], any] => {
    condition = $$.isFunction(condition) ? condition : (item: DataRowType) => {return true};
    const
        pageSize: number = actualPositive(view.pageSize, wholeData.length),
        windowNumberRows: number = actualPositive(view.windowNumberRows, wholeData.length),
        pageNumber: number = view.page,
        res: RenderRecord[] = [];       // result - for displaying

    if (selectedIdx.length === 0) {
      // Initial filtering
      dataIteratorFilter(selectedIdx, wholeData, view, condition);
    }
    // let startRow = 0;
    let pageFirstRow = Math.max(0, pageNumber * pageSize);

    const selectedLen = selectedIdx.length;
    const maxWindowStart = selectedLen - windowNumberRows;

    // automatic PAGE_RESIZE support
    if (view.sys.lastAction === 'PAGE_RESIZE') {
        // view.sys.lastAction = null;  // not used yet
    }

    const countPages = Math.ceil(selectedLen / pageSize);
    view.page = view.page >= countPages ? countPages -1 : view.page;

    if (pageFirstRow + pageSize > selectedLen) {
        pageFirstRow = Math.max(0, selectedLen - pageSize);
    }

    const scrollRate = windowNumberRows / selectedLen;

    const scrollShift = view.scrollShift || 0;

    let startRow = Math.max(0, pageFirstRow + scrollShift);

    if (startRow + windowNumberRows > selectedLen) {
        startRow = Math.max(0, maxWindowStart);
    }
    // console.log('**###START ROW::[' + startRow + '] scrollShift::[' + scrollShift + ']')
    const resultInfo = {} as DataRenderResultInfo;

    if (view.page === -1) {
        view.page = 0;
        res.push(new RenderRecord(rowFormatter(view.emptyRecord, 0, {
                page: 0,
                pages: 0,
                // count: 0,
                rowIndex: 0,
                first: true,
                last: true,
                empty: true
            })));
    }
    else {
        // collecting ROWS IN TO RESULTED OUTPUT
        let rows = [];

        for (let i = startRow, cnt = windowNumberRows; i < selectedLen && cnt > 0; i += 1, cnt -= 1) {
            const index = selectedIdx[i],
                row = wholeData[index];
            if (! $$.isDefined(row)) {
               break;
            }
            row._idx_ = index;
            rows.push(row);
        }

        // console.log('######################### rows.size::', rows.length);

        if ($$.isArray(view.sorter.applied)) {
            rows = dataSorter.applySort(view.sorter, rows)
        }
        for (let i= 0; i < rows.length; i += 1) {
            const row: DataRowType = rows[i];
            const page= view.page;
            res.push(new RenderRecord(rowFormatter(row, i, {
                page: page,
                pages: countPages,
                rowIndex: i,
                first: i === 0,
                last: i === selectedLen - 1
            })));
        }
        view.pageInfo = {page: view.page + 1, pages: countPages};
    }

    // view.actualCountOfRows = selectedLen;

    if ($$.isFunction(view.sorter.onceSorter)) {
        res.sort(view.sorter.onceSorter);
        view.sorter.onceSorter = null
    }

    view.actualPageSize = pageSize;
    // view.actualCountOfRows = selectedLen;
    // view.scrollShift = scrollShift;
    // view.scrollBarSizePercentageY = Math.min(1,  view.windowNumberRows / pageSize);
    // view.scrollPercentage.y = Math.min(1, scrollShift / (pageSize - view.windowNumberRows));

    return [res, resultInfo];
};
