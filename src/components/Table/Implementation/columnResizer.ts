// @see https://www.brainbell.com/javascript/making-resizable-table-js.html

import {$$} from 'src/system/utils'
import { COLUMN_RESIZE, TABLE_RESIZE, MouseEvent, TABLE_RESIZE_X } from 'components/Table/Types/EventTypes';

const SEPARATOR_STYLE = '4px solid #00B4FF';

export const columnResizer = {
    createControlDiv(height: number, deltaWidth = 8, columnSelector = 'columnSelector'): HTMLElement {
        const div = document.createElement('div');
        div.style.top = '0';
        div.style.right = '0';
        div.style.width = '' + deltaWidth + 'px';
        div.style.position = 'absolute';
        div.style.cursor = 'col-resize';
        div.style.userSelect = 'none';
        div.style.height = height + 'px';
        div.className = columnSelector;
        return div;
    },

    setListeners(columnHeader: any,
                 divElement: HTMLElement,
                 columnElement: HTMLElement,
                 indexOfColumn: number,
                 columnsWidth: number[],
                 isLastItem: boolean,
                 tableDom: HTMLElement,
                 tableRefId: string): void {
        let pageX: number | undefined,
            curCol: HTMLElement | undefined,
            curColWidth: number | undefined;
        const columnIndex = indexOfColumn;
        const tableColumnsWidth = columnsWidth
        const tableMinWidthStr = tableDom.style.minWidth;
        let tableMinWidthBase = parseInt(tableDom.style.minWidth.substring(0, tableMinWidthStr.length - 'px'.length));
        let tmpTabWidth: number = tableMinWidthBase;

        columnElement.style.width = columnsWidth[columnIndex] + 'px';

        divElement.addEventListener('mousedown', function (e) {
            if (e && e.target) {
                curCol = divElement.parentElement as HTMLElement;
                pageX = e.pageX;
                curColWidth = curCol.offsetWidth;
                divElement.style.borderRight = SEPARATOR_STYLE;
            }
        });

        document.addEventListener('mousemove', (e: MouseEvent) => {
            if (curCol && curColWidth && pageX) {
                const diffX = e.pageX - pageX;
                const newCurSize = curColWidth + diffX
                const newTableSize = tableMinWidthBase + diffX;

                tableColumnsWidth[columnIndex] = newCurSize;
                tmpTabWidth = newTableSize;
                curCol.style.minWidth = `${newCurSize}px`;
                tableDom.style.minWidth = `${newTableSize}px`;
                const eventData = {
                  'tableWidth': newTableSize,
                  'columnWidth': newCurSize,
                  'columnIndex': columnIndex,
                };
                $$.raiseEvent(TABLE_RESIZE_X, undefined, eventData);
              }
        });

        document.addEventListener('mouseup',  e => {
            curCol = undefined;
            pageX = undefined;
            curColWidth = undefined;
            tableMinWidthBase = tmpTabWidth;
            divElement.style.removeProperty('border-right');
        });
    }
};
