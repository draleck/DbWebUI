import { $$ } from 'src/system/utils';

import { dataSorter } from './dataSorter'
import { dataPager } from './dataPager'
import dataSearch from './dataSearch';

import { DEFAULT_WINDOW_ROWS, Point, SearchViewModes } from 'components/Table/Types/TableViewType';
import { BarXYPercentage } from 'components/Table/Implementation/dataScrollBar';
import { wheelDelta, wheelScale } from 'components/Table/Implementation/dataScrollWheel';
import { isControlKeyPressed, isMetaKeyPressed } from 'components/system/ActionWrapper.vue';
import { actualPositive, dataIteratorFilter } from 'components/Table/Implementation/dataIterator';
import { PropType } from 'vue';


class Iterator {
    index: number;
    constructor() {
        this.index = -1;
    }
}

export default {
    // selectedIdx: Array as PropType<number[]> = [],
    // selectedIdx: [],
    methods: {
        registerTable(component: any) {
            if ($$.isDefined(this.context)) {
                const iname = $$.safeGet(this, ['context', 'tables', this.iname]);
                if (! $$.isDefined(iname)) {
                    $$.safeSet(this, ['context', 'tables', this.iname], component);
                    $$.safeSet(this, ['context', 'iterator', this.iname], this.createIterator())
                }
            }
        },
        tableBaseHeight() {
          const initialHeight = this.initialColumnHeight
          const initialTotalHeight = initialHeight * DEFAULT_WINDOW_ROWS;  // 20 default number of columns
          return initialTotalHeight;
        },
        windowSizeReCalculate(scaleNew: number) {
          const winSizeY =  this.windowSize.y;

          const initialHeight = this.initialColumnHeight;
          const currentHeight = initialHeight * scaleNew;
          // alert('initialHeight::' + initialHeight + ' current::' + currentHeight)
          // const initialTotalHeight = initialHeight * DEFAULT_WINDOW_ROWS;  // 20 default number of columns
          const initialTotalHeight = this.tableBaseHeight();
          const newCountOfRows = Math.round(initialTotalHeight / currentHeight)
          //DEFAULT_ROW_HEIGHT
          // alert('initialHeight::' + initialHeight + ' current::' + currentHeight + ' newCountOfRows::' + newCountOfRows)
          // this.setRowHeight(currentHeight);
          // console.log('initialHeight::' + initialHeight + ' current::[' + currentHeight + '] newCountOfRows::' + newCountOfRows+ '... initialTotalHeight::' + initialTotalHeight)
          if (newCountOfRows !== this.view.windowNumberRows) {
              this.$nextTick(() => {
                this.setWindowNumberRows(newCountOfRows);
                 this.setRowHeight(currentHeight);
              })
          }
          return 0;
        },
        createIterator()    { return new Iterator() },
        actualCountOfRows() { return this.view.actualCountOfRows },

        initSorter ()     { dataSorter.initSorter(this.view, this.model) },
        sortDir (key: any){ dataSorter.sortDir(this.view, key) },
        shuffle ()        { dataSorter.sortRandom(this.view) },
        goPage(data: any) { dataPager.goPage(this.view, data.mode, data.value) },

        setScale(scale: number){
          this.windowSizeReCalculate(scale);
          this.view.textScale = scale;
        },
        setFirstRow (rowNumber: number) { this.view.scrollShift(rowNumber) },

        doSearch(data: any)               { dataSearch.doSearch(this.view, data, this.model) },
        searchMode(mode: SearchViewModes) { dataSearch.searchMode(this.view, mode) },
        clearSearch(data: any): void            { dataSearch.clearSearch(this.view, data) },

        setWheelDelta(delta: number): void {
            const controlKeyPressed = isControlKeyPressed() || isMetaKeyPressed();
            if (controlKeyPressed) {
              this.setScale(wheelScale(this.view.textScale, wheelDelta(this.view, delta)));
            }
            else {
              this.view.scroll.wheelDelta = delta;
              this.recalculateTableView();
            }
        },

        setRowPercentage(percentage: BarXYPercentage) {
            this.view.scrollPercentage = percentage;
            const winRows = this.view.windowNumberRows,
                pageSize  = this.view.actualPageSize;
            this.view.scrollShift = Math.round((pageSize - winRows)  * this.view.scrollPercentage.y)
        },

        setWindowNumberRows(countOfRows: number): void {
          this.view.windowNumberRows = countOfRows;
        },
        setRowHeight(rowHeight: number): void {
          this.view.rowHeight = rowHeight;
        },
        setPageSize(size: number): void { dataPager.setPageSize(this.view, size) },
        getPageSize(): number   { return dataPager.getPageSize(this.view) },
        currentPage(): number   { return dataPager.currentPage(this.view) },
        countPages(): number    { return dataPager.countPages(this.view) },

        registerRow(rowIndex: number, isLastRow: boolean, columnsWidth: Array<number>, columHeight: number) {
            const notRegistered = this.columnsWidth.length === 0;
            if (isLastRow && notRegistered) {
                const summa = columnsWidth.reduce((sum, current) => sum + current, 0);
                this.columnsWidthSumma = summa;
                this.columnsWidth = Object.assign(columnsWidth);
                if (this.initialColumnHeight === 0) {
                // if (true) { // -1 == header
                  this.initialColumnHeight = columHeight;
                  this.initialWindowNumberRows = rowIndex;

                  // console.log('$$$$$$$$$$$$$$$$ initialColumnHeight:: >>>', this.initialColumnHeight)
                  // console.log('$$$$$$$$$$$$$$$$ initialWindowNumberRows::>>>', this.initialWindowNumberRows)
                }

                // columnsWidth.forEach(width => {
                //     const widthPercent = width / summa * 100;
                //     // this.columnsWidth.push('' + widthPercent + '%');
                //     // this.columnsWidth.push('' + width + 'px');
                //     this.columnsWidth.push(width);
                // });

                // console.log('****** REGISTER - LAST COLUMN ****** summa: [' + summa + ']', this.columnsWidth)
            }
        },
        recalculateTableView () : void {
          // console.log('############# re-calculate')

          const view = this.view;
          dataIteratorFilter(this.selectedIdx, this.inputData, view);
          const count_of_visible_rows = this.selectedIdx.length;

          // const wholeData = this.inputData || []
          const pageSize: number = actualPositive(view.pageSize, count_of_visible_rows);
          const windowNumberRows: number = actualPositive(view.windowNumberRows, count_of_visible_rows);
          let scrollShift = view.scrollShift;
        // if (scrollRate < 1) {
            const delta = wheelDelta(view, view.scroll.wheelDelta);
            scrollShift += delta;
        // }
          scrollShift = Math.max(0, scrollShift);
          scrollShift = Math.min(scrollShift, pageSize - windowNumberRows);

          view.actualCountOfRows = count_of_visible_rows;
          view.scrollShift = scrollShift;
          view.scrollBarSizePercentageY = Math.min(1,  view.windowNumberRows / pageSize);
          view.scrollPercentage.y = Math.min(1, scrollShift / (pageSize - view.windowNumberRows));
        }

        // OK BUT NOT USED
        // registerRow(index, isLast) {
        //     if (isLast) {
        //         this.checkHeight();
        //     }
        // },
        //
        // checkHeight() {
        //     const newHeight = this.asDomElement().clientHeight;
        //     if (this.settings.tableHeight !== newHeight) {
        //         $$.raiseEvent('TABLE-RESIZE', {refId: this.selfRefName});
        //     }
        //     this.settings.tableHeight = newHeight;
        // },
        //
        // // not used - can be removed
        // registerRowBox(box, index, isLast): void {
        //     this.domInfo.rowBoxes[index] = box;
        //     if (isLast) {
        //         // this.domRecalculate();
        //         this.checkHeight();
        //     }
        // },
        // // not used - can be removed
        // domRecalculate() {
        //     let newHeight = 0;
        //     let newWidth = 0;
        //     for (let i=0; i< this.domInfo.rowBoxes.length; i+=1) {
        //         const box = this.domInfo.rowBoxes[i];
        //         newHeight += box.height;
        //         newWidth += box.width;
        //     }
        //     if (this.domInfo.currentHeight !== -1) {
        //         if (newHeight != this.domInfo.currentHeight || newWidth != this.domInfo.currentWidth) {
        //             $$.raiseEvent('TABLE-RESIZE', {refId: this.selfRefName});
        //         }
        //     }
        //
        //     this.domInfo.currentHeight = newHeight;
        //     this.domInfo.currentWidth = newWidth;
        //     return null
        // }
    },

    computed: {
        // aggregated() {
        //     const summup = this.aggregatedValues = [];
        //     this.aggregate_method.forEach((action, i) => {
        //             summup.push(action);
        //             if (action.action === 'COUNT') {
        //                 summup[i].value = {
        //                 'SUM': 0,
        //                 'AVR': 0,
        //                 'MIN': Number.MAX_SAFE_INTEGER,
        //                 'MAX': Number.MIN_SAFE_INTEGER,
        //                 'COUNT': 0
        //                 }
        //             }
        //         });
        //
        //     this.items.forEach((item: { data: { [x: string]: number; }; }) => {
        //         summup.forEach((action, i) => {
        //             const field = this.model[i];
        //
        //           // @ts-ignore who knows why
        //           if ($$.isIncludes(['COUNT', 'SUM', 'AVR', 'MIN', 'MAX'], action.action)) {
        //                 summup[i].value.COUNT += 1;
        //                 summup[i].value.SUM += item.data[field.name];
        //                 summup[i].value.MIN = Math.min(summup[i].value.MIN, item.data[field.name]);
        //                 summup[i].value.MAX = Math.max(summup[i].value.MAX, item.data[field.name]);
        //             }
        //         })
        //     });
        //
        //     summup.forEach((action, i) => {
        //         if ($$.isIncludes(['COUNT', 'AVR'], action.action)) {
        //             summup[i].value.AVR = summup[i].value.SUM / summup[i].value.COUNT
        //         }
        //     });
        //     return this.aggregatedValues
        // },
        iterator()  { return this.context.iterator[this.iname]},
    },
    data() {
        return {
            columnsWidthBefore: [] as number[],
            columnsWidth: [] as number[],
            columnsWidthSumma: 0,
            windowSize: new Point(-1, -1),
            initialColumnHeight: 20,
            currentColumnHeight: 20,
            initialWindowNumberRows: 0,
            aggregatedValues: [],
            domInfo: {
                currentHeight: -1,
                currentWidth: -1,
                rowBoxes: [],
            }
        }
    },
    created () {
        this.registerTable(this);
    },
}
