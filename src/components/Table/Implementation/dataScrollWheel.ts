import { TableViewType } from 'components/Table/Types/TableViewType';

export default {
    mounted() {
        this.$refs[this.selfRefName].addEventListener('wheel', (e: any) => {
            e.stopPropagation();
            e.preventDefault();
            this.setWheelDelta(e.deltaY);
        });
    }
}

export const wheelDelta = (view: TableViewType, wheelShift: number): number => {
        const wheelRate = view.scroll.wheelRate;
        const deltaValue = Math.round(Math.abs(wheelShift) / wheelRate);
        return wheelShift > 0 ? deltaValue : -deltaValue;
}

export const wheelScale = (current: number, delta: number) => {
    const min_scale = 0.4;
    const max_scale = 2.6;
    let scale = current * (100 - delta) / 100;
    scale = Math.max(min_scale, scale)
    scale = Math.min(max_scale, scale)
    return scale;
}
