import dataFilter from './dataFilter'
import dataCommon from './dataCommon'
import {SearchViewModes, TableViewType} from 'components/Table/Types/TableViewType';
import { TableColumnModelType } from 'components/Table/Types/TableColumnType';

const SEARCH_MODE_FILTER = 'search';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const handleSearch = (view: TableViewType, {text, params}, model: TableColumnModelType[]) => {
    const mode = params.mode,
        sensitive = params.sensitive;

    if (model) {
        text = sensitive ? text : text.toLowerCase();
        dataFilter.removeAllAutoFilters(view, 'search');

        const searchable = dataCommon.interestedFields(model, params);

        for (let i = 0; i < searchable.length; i += 1) {
            const field = searchable[i].trim();
            if (field) {
                dataFilter.addFilter(view, {
                    key: field,
                    value: text,
                    filterMode: SEARCH_MODE_FILTER,
                    filterAction: mode,
                    filterSensitive: sensitive
                })
            }
        }
    }
};

export const clearSearch =  (view: TableViewType, text: string): void => {
    dataFilter.removeAllAutoFilters(view, SEARCH_MODE_FILTER);
};

export const searchMode = (view: TableViewType, mode: SearchViewModes): void => {
    console.log('searchMode [' + mode + ']')
};

export default {
    doSearch: handleSearch,
    clearSearch: clearSearch,
    searchMode: searchMode
}
