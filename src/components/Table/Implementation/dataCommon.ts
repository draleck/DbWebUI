import { $$ } from 'src/system/utils';
import {TableColumnModelType} from 'components/Table/Types/TableColumnType';

const interestedFields = (model: TableColumnModelType[], aspect: any): string[] =>  {
    const result = [] as string[];
    if ($$.isArray(model) && $$.isArray(aspect.types)) {
        model.forEach(field => {
            let fieldType   = $$.isDefined(field.type) ? $$.toLower(field.type) : ''
            const fieldName = $$.isDefined(field.name) ? $$.toLower(field.name) : null
            fieldType = $$.toLower(fieldType.replace(/[^a-z|A-Z]/g,''));
            if ($$.isIncludes(aspect.types, fieldType)
             || $$.isIncludes(aspect.fields, fieldName)) {
                result.push(field.name)
            }
        })
    }
    $$.checkedInsert(result, aspect.fields);
    return result
};

const isNumberField = (typeStr: string, fieldName: string): boolean => {
    typeStr = $$.toLower(typeStr);
    fieldName = $$.toLower(fieldName);

    if (fieldName === 'id'
        || $$.lastChars(fieldName, 3) === '_id'
        || $$.firstChars(fieldName, 3) === 'is_' ) {
        return false
    }

    return $$.firstChars(typeStr, 3) === 'int'
        || $$.isIncludes(['uint', 'floa', 'doub'], $$.firstChars(typeStr, 4))
};


export default {
    interestedFields: interestedFields,
    isNumberField: isNumberField,
}
