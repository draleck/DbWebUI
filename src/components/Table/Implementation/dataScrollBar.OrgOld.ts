import { $$ } from 'src/system/utils';

export interface MoveDirection {
    hor: number;
    ver: number;
    calculatePercentage?: boolean;
}

export interface BarXYPercentage {
    x: number;
    y: number;
    auto?: boolean;
}

export interface RectBox {
    x: number;
    y: number;
    top: number;
    bottom: number;
    left: number;
    right: number;
    height: number;
    width: number;
}

export default {
    emits:['scrollBarSize', 'scroll'],
    methods: {
        applyBarSize(): void {
            this.adjustBarSize(this.controller.view.scrollBarSizePercentageY);
        },

        adjustBarSize(barSizePercentY: number): void {
            const wrapperRect = this.wrapperRectBox,
                baseHeight = wrapperRect.height,
                barHeight = baseHeight * barSizePercentY;
            const size = Math.max(this.controller.view.scroll.minScrollBarSize, barHeight);
            this.bar.style.height = size + 'px';
            this.currentScrollBarSize = size;
            this.$emit('scrollBarSize', size);
        },

        adjustBarPosit(yPercent: number, applyMove = false): number {
            const halfHeight =  ('' + this.bar.style.height).slice(0, -2);
            const wrapperRect = this.wrapperRectBox,
                baseHeight = wrapperRect.height,
                barHeight = parseInt(halfHeight),
                barHalfHeight = barHeight / 2;

            const preSize  = Math.max(0, baseHeight * yPercent - barHalfHeight);
            const postSize = Math.max(0, baseHeight - preSize - barHeight);

            const delta = preSize + postSize + barHeight - baseHeight;
            if (applyMove) {
                this.setElementHeight(this.preBar, preSize - delta);
                this.setElementHeight(this.postBar, postSize);
            }

            return preSize === 0 ? 0 : postSize === 0 ? 1 : yPercent;
        },

        getMovePercent(mode: MoveDirection, x: number, y: number): number {
            const wrapperRect = this.wrapperRectBox,
                baseHeight = wrapperRect.height,
                minTop = wrapperRect.top;

            return (y - minTop) / baseHeight;
        },

        buildVerticalScroll(percentage: BarXYPercentage): void {
            this.draggableScrollBar(percentage, {ver: 1, hor: 0});
        },

        setElementHeight (element: HTMLElement, size: number): void {
          if (element) {
            element.style.height = size + 'px';
          }
        },

        emitPercentage(percentage: BarXYPercentage) {
            console.log('####### emitPercentage: percentage::', percentage.y)
            this.controller.onScroll(percentage)
        },

        resetPercentage()  {
          this.$emit('scroll', {x: 0, y: 0} as BarXYPercentage);
        },

        draggableScrollBar(percentage: BarXYPercentage, mode: MoveDirection): boolean | void {
            mode = $$.isDefined(mode) ? mode : {ver: 1, hor: 1} as MoveDirection;

            this.wrapperRectBox = this.wrapper.getBoundingClientRect();
            this.adjustBarPosit(percentage.y, true);

            const mousePointMove = (event: MouseEvent) => {
                event.stopPropagation();
                event.preventDefault();
                const percentageY = this.getMovePercent(mode, event.pageX, event.pageY);
                this.emitPercentage({y: percentageY, x: 0});
            };

            document.removeEventListener('mousemove', mousePointMove);

            this.bar.onmousedown = (event: MouseEvent) => {
                event.stopPropagation();
                event.preventDefault();
                document.addEventListener('mousemove', mousePointMove);

                window.onmouseup = () => {
                    document.removeEventListener('mousemove', mousePointMove);
                    window.onmouseup = null;
                }
            };
            this.wrapper.onmousedown = mousePointMove
        }
    }
}
