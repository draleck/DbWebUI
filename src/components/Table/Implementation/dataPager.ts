import {TableViewType} from 'components/Table/Types/TableViewType';
import {$$} from 'src/system/utils';

// Internal Pages enumerations - from '0'

export const dataPager = {
    goPage(view: TableViewType, mode: 'left' | 'right' | 'home' | 'end' | 'value', value= -1 ) {
        console.log('dataPager::goPage')
        const modes = {
            'left':  (value: number) => view.page = Math.max(0, view.page - 1) ,
            'right': (value: number) => view.page = view.page + 1,
            'home':  (value: number) => view.page = 0 ,
            'value': (value: number) => view.page = $$.isDefined(value) ? value  : 0,
            'end':   (value: number) => view.page = this.countPages(view)
            };

        if (value !== -1) {
          modes[mode](value);
        }
        view.scroll.indexY = 0;
    },
    setPageSize(view: TableViewType, size: number): void {
        view.pageSize = size;
        view.sys.lastAction = 'PAGE_RESIZE';
    },
    countPages(view: any): number {
        console.log('view.actualCountOfRows', view.actualCountOfRows)
        console.log('view.pageSize', view.pageSize)
        return Math.max(0, Math.floor((view.actualCountOfRows + view.pageSize -1) / view.pageSize));
    },
    getPageSize(view: TableViewType): number   { return view.pageSize > 0 ? view.pageSize : view.actualCountOfRows },
    currentPage(view: TableViewType): number   { return view.page }
};
