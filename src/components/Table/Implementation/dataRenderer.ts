import { $$ } from 'src/system/utils';

import { DataRowType } from '../Types/TableViewType'
import { TableViewType } from '../Types/TableViewType'
import { RenderRecord } from '../Types/TableViewType'
import { RenderRecordSystemInfoInterface } from '../Types/TableViewType'
import { RenderRecordInterface } from '../Types/TableViewType'
import { RenderRecordSystemInfo } from '../Types/TableViewType'
import {TableColumnModelType} from 'components/Table/Types/TableColumnType';

export const dataRender = (
        selectedIdx: Array<number>,
        wholeData: DataRowType[],
        model: TableColumnModelType[],
        view: TableViewType,
        settings: Record<string, any>,
        // iterator: (d: DataRowType[], w: TableViewType, f: any, c: any) => void,
        // eslint-disable-next-line @typescript-eslint/ban-types
        iterator: Function,
        // eslint-disable-next-line @typescript-eslint/ban-types
        postprocessor: Function | null,
        condition: any): [RenderRecordInterface[], any] =>
    {
    const pageSize = view.pageSize,
        addinfo = view.addinfo;
        //previous_data = {SYS: {}, data: {}, CONTEXT: {}, old: {}},
        //perform = $$.isFunction(postprocessor) ? postprocessor : row => { return row };

    return iterator(
        wholeData,
        selectedIdx,
        view,
        (row: DataRowType, index: number, context: RenderRecordSystemInfoInterface): RenderRecordInterface => {
            const   currentIndex = context.rowIndex,
                    wholeIndex = context.page * pageSize + index + 1,
                    pageIndex = currentIndex + 1  % pageSize - 1;

        const systemInfo = new RenderRecordSystemInfo({
                    'rowIndex': currentIndex,
                    'rowNumber': currentIndex + 1,
                    'indexData': wholeIndex,
                    'indexPage': pageIndex,
                    // 'index_actual':  row._i_ ? row._i_ + 1 : 0,
                    'indexActual':  0,
                    'even': pageIndex % 2 === 0,
                    'odd':  pageIndex % 2 !== 0,
                    'empty': context.empty,
                    'first': context.first,
                    'last': context.last,
                    'inner': ! (context.last || context.first),
                    'pagesize': pageSize,
                    'page': context.page,
                    'pages': context.pages
        });
        const data: RenderRecordInterface = new RenderRecord({
                key: $$.uniqueId('key'),
                SYS: systemInfo,
                CONTEXT: view.context,
                data: row,
                addinfo: addinfo,
                settings: settings,
                model: model,
                actualNumberOfColumns: $$.isDefined(row) ? Object.keys(row).length : 0,
                //old: previous_data
            });
        // previous_data = data;
        return data;
        //return perform(data)
    }, condition)

};
