import { $$ } from 'src/system/utils';
import {DataRowType, TableViewType, SorterItem, SorterType} from '../Types/TableViewType'

import dataCommon from './dataCommon'
import { TableColumnModelType } from 'components/Table/Types/TableColumnType';

const sortRandom = (view: TableViewType) => {
    view.sorter.onceSorter = (a:any, b:any) => {
        return Math.floor(Math.random() * 10) > 4 ? 1 : -1
    }
};

const initSorter = (view: TableViewType, model: TableColumnModelType[]) => {
    dataCommon.interestedFields(model, view.sorter).forEach( name => {

      console.log('ATTENTIN -> NEED TO fix')
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      view.sorter.sortable[name] = new SorterItem({'key': name, direction: 'down'})
    })
};

const sortDir = (view: TableViewType, key: number) => {
    return view.sorter.sortable[key]
};

const applySort = (sorter: SorterType, data: DataRowType[]): DataRowType[] => {
    const applied = sorter.applied;

    if ($$.isArray(applied) && applied.length > 0) {
        const appliedSorter = applied[0],     // for now only a single sorter are applied
            sortDir = appliedSorter.direction,
            key = appliedSorter.key,
            ab = sortDir === 'down' ? 1 : -1,
            ba = sortDir === 'down' ? -1 : 1;


        console.log('ATTENTIN -> NEED TO fix')
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
        sorter.sortable[key] = {key: key, direction: sortDir};

        const compareSimple = (a: any, b: any) => {
            if (a[key] < b[key]) { return ab }
            if (a[key] > b[key]) { return ba }
            return 0;
        };
        const compareDotted = (a: any, b: any) => {
            if ($$.getDottedValue(key, a) < $$.getDottedValue(key, b)) { return ab }
            if ($$.getDottedValue(key, a) > $$.getDottedValue(key, b)) { return ba }
            return 0;
        };
        data.sort(key.indexOf('.') === -1 ? compareSimple : compareDotted);
    }
    return data
};

export const dataSorter = {
    sortDir: sortDir,
    initSorter: initSorter,
    sortRandom: sortRandom,
    applySort: applySort
};
