import { $$ } from 'src/system/utils';

export interface MoveDirection {
    hor: number;
    ver: number;
    calculatePercentage?: boolean;
}

export interface BarXYPercentage {
    x: number;
    y: number;
    auto?: boolean;
}

export interface RectBox {
    x: number;
    y: number;
    top: number;
    bottom: number;
    left: number;
    right: number;
    height: number;
    width: number;
}

export default {
    emits:['scrollBarSize', 'scroll'],
    methods: {
        getMovePercent(mode: MoveDirection, x: number, y: number): number {
            const wrapperRect = this.wrapperRectBox,
                baseHeight = wrapperRect.height,
                minTop = wrapperRect.top;

            return (y - minTop) / baseHeight;
        },

        buildVerticalScroll(): void {
            const height = this.draggableScrollBar({ver: 1, hor: 0});
            if (this.tableHeight !== 0) {
              this.controller.tableHeight = height;
              // console.log('########### Height set  up [' + height + ']')
            }
        },

        emitPercentage(percentage: BarXYPercentage) {
            // console.log('####### emitPercentage: percentage::', percentage.y)
            this.controller.onScroll(percentage)
        },

        draggableScrollBar(mode: MoveDirection): boolean | void {
            mode = $$.isDefined(mode) ? mode : {ver: 1, hor: 1} as MoveDirection;

            const wrapperRectBox =  this.wrapper.getBoundingClientRect();
            this.wrapperRectBox =  {
              height: wrapperRectBox.height,
              top: wrapperRectBox.top
            }

            const mousePointMove = (event: MouseEvent) => {
                event.stopPropagation();
                event.preventDefault();
                const percentageY = this.getMovePercent(mode, event.pageX, event.pageY);
                this.emitPercentage({y: percentageY, x: 0});
            };

            document.removeEventListener('mousemove', mousePointMove);

            this.bar.onmousedown = (event: MouseEvent) => {
                event.stopPropagation();
                event.preventDefault();
                document.addEventListener('mousemove', mousePointMove);

                window.onmouseup = () => {
                    document.removeEventListener('mousemove', mousePointMove);
                    window.onmouseup = null;
                }
            };
            this.wrapper.onmousedown = mousePointMove;
            return wrapperRectBox.height;
        }
    }
}
