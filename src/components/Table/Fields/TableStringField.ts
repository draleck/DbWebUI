import { h } from 'vue'

export const getStyle = (textScale: number, style: any) => ({
          ...style,
          'font-size': textScale * 100 + '%',
        })
export const TableStringField = (props: {item: any, wholeData: any, itemIndex: number, textScale: number, style: any}) => {
  // console.log('@ props.wholeData::', props.wholeData)
  return h('span', {style: getStyle(props.textScale, props.style)}, `${props.item.value}`)
}
