
import {WsConnector} from 'components/system/ws-connector';
import { wsAddMsg } from './ws-request-queue'
import { IWsIdData } from 'src/app/DbBrowser/myinterfaces';
import { testBinanceWSData, dataReceiveQueue, wsConnStatus, answCatalog, porocessingSqlAnswer, answFetch, tableModel, getTableModel, currentConnection } from 'src/app/DbBrowser/myutils';

const wsConnector = new WsConnector('');

const wsPerformers = {} as Record<string, (data: any) => void>

export const initWebsocket = (url: string) => {
  if (!wsConnector.ready) {
    wsConnector.url = url;

    wsConnector.init_receiver((wsPayload: any) => {
      // console.log('################# WS PAYLOAD::', JSON.parse(wsPayload.data || '{}'))
      const data = JSON.parse(wsPayload.data || '{}')
      const action = data['mode'];
      const payload = data['data'];
      wsAddMsg(data);

      // testBinanceWSData.value = payload;


      // if (data['is_active']) Object.keys(data['is_active']).map(key => wsConnStatus.value[key] = data['is_active'][key]);
      // const uniqueId = Object.keys(data)[0] || '';

      // if (uniqueId.length > 0) {

      //   switch (Object.keys(data[uniqueId])[0]) {
      //     case 'row_count':
      //       answCatalog.value.forEach((item: {name: string, row_count: number}) => {
      //         if (item['name'] === Object.keys(data[uniqueId]['row_count'])[0]) item['row_count'] = Number(data[uniqueId]['row_count'][item['name']])
      //       })
      //       break;
      //     default:
      //       if (dataReceiveQueue.value[uniqueId]) {
      //         switch (dataReceiveQueue.value[uniqueId]) {
      //           case 'catalog':
      //               answCatalog.value = Object.keys(data[uniqueId]['catalog']).length === 0
      //                 ? []
      //                 : data[uniqueId]['catalog'];
      //                 break;
      //           case 'fetch':
      //               // console.log('!!! WS fetch case ', data[uniqueId])
      //               const val = porocessingSqlAnswer(data[uniqueId]);
      //               // console.log('!!! Result val ', val)
      //               answFetch.value = val.rows;
      //               tableModel.value = getTableModel(val.meta.model);
      //               currentConnection.catalog.sql = val.meta['query'] || '';
      //               // console.log('!!! Result AFTER ', val)
      //               break;
      //         };
      //         delete dataReceiveQueue.value[uniqueId];
      //       }
      //   }
      // }

      const performer: any = wsPerformers[action];
      if (!performer) {
        // console.log('WS message for action [' + action + '] Skipped')
      } else {
        performer(payload);
      }
    });
  }
}


export const websocketSend = (message: any) => {
    wsConnector.send(message);
}

export const registerWebsocketListener = (action: string, performer: (data: any) => void) => {
  wsPerformers[action] = performer;
}

export const cancelWebsocketListener = (action: string, performer: (data: any) => void) => {
  if(wsPerformers[action]) {
    delete wsPerformers[action];
  }
}


