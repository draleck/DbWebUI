import { IResponseForTable } from 'src/app/DbBrowser/myinterfaces';
import { answCatalog, answFetch, currentConnection, getTableModel, porocessingSqlAnswer, tableModel, wsConnStatus } from 'src/app/DbBrowser/myutils';
import { ref, reactive, watch } from 'vue';

interface IWsMsg {
  unique_id?: string,
	action: string,
  // keyField: string,
  // keyValue: string
  data: {
    [fieldName: string]: any
  }
}

interface IWsMsgQueue {
  [unique_id: string]: IWsMsg[]
}

interface ITablesUUID {
  [tableName: string]: string,
}

const wsQueue: IWsMsgQueue = {};

export const tablesUUID = reactive<ITablesUUID>({
  catalog: '',
  fetch: ''
});

const wsExecMsg = (wsMsg: IWsMsg) => {
  switch (wsMsg.action) {
    case 'conn_activity':

      wsMsg['data'].map((item: any) => {
        // console.log('[WS] conn_activity ', Boolean(item.fields['is_active']));
        wsConnStatus.value[item.keyValue] = item.fields['is_active'] === null ? null : Boolean(item.fields['is_active'])
      })
      break;
    default:
  }

}

const getActionUUID = (uuid: string, tblUUID: ITablesUUID): string | undefined => {
  let result: string | undefined = undefined;
  Object.keys(tblUUID).forEach((action: string) => {
    if (tblUUID[action] === uuid) result = action
  })
  return result;
}

const wsProcRequest = (msgAction: string, wsMsg: IWsMsg) => {
    // console.log('[WS] wsProcRequest msgAction: ', msgAction);
    // console.log('[WS] wsProcRequest wsMsg: ', wsMsg);
    switch (msgAction) {
      case 'catalog':
        switch (wsMsg.action) {
          case 'catalog':
            answCatalog.value = Object.keys(wsMsg.data['catalog']).length === 0
              ? []
              : wsMsg.data['catalog'];
            break;
          case 'fields':
            console.log('[WS] fields action ');
            wsMsg['data'].map((item: any) => {
              // console.log('[WS] conn_activity ', Boolean(item.fields['is_active']));
              for (let i = 0; i < answCatalog.value.length; i++) {
                if (answCatalog.value[i][item['keyField']] === item['keyValue']) {
                  Object.keys(item['fields']).map(key => {
                    answCatalog.value[i][key] = item['fields'][key];
                  })
                }
              }

            })
            break;
        }
        break;
      case 'fetch':
        const val = porocessingSqlAnswer(wsMsg.data as IResponseForTable);
        // console.log('!!! Result val ', val)
        answFetch.value = val.rows;
        tableModel.value = getTableModel(val.meta.model);
        currentConnection.catalog.sql = val.meta['query'] || '';
        break;
    }
}

export const wsAddMsg = (wsMsg: IWsMsg) => {
  switch (wsMsg.action) {
    case 'conn_activity': wsExecMsg(wsMsg); break;
    default:
      if (wsMsg.unique_id) {
        // console.log('[WS] getActionUUID ', getActionUUID(wsMsg.unique_id));
        const msgAction = getActionUUID(wsMsg.unique_id, tablesUUID);
        if (msgAction) wsProcRequest(msgAction, wsMsg)
          else {
            if (!wsQueue[wsMsg.unique_id]) wsQueue[wsMsg.unique_id] = [];
            wsQueue[wsMsg.unique_id].push(wsMsg)
            // console.log('[WS] New msg added to wsQueue ', wsQueue);
        }
      } else console.error('[WS] No unique_id!!');
  }
}

const wsReProcRequest = (tblUUID: ITablesUUID) => {
  Object.keys(tblUUID).forEach((action: string) => {
    const uuid = tblUUID[action];
    if (wsQueue[uuid]) {
      // console.log('[WS] wsReProcRequest ', uuid)
      wsQueue[uuid].forEach((msg: IWsMsg) => wsProcRequest(action, msg))
      delete wsQueue[uuid];
    }
  })
}

watch(tablesUUID, (newVal: ITablesUUID) => {
  // console.log('[WS] new tablesUUID', newVal)
  if (Object.keys(wsQueue).length > 0) wsReProcRequest(newVal);
});
