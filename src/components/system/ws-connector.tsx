import ReconnectingWebSocket from 'reconnecting-websocket';

export class WsConnector {
    url: string;
    ws: ReconnectingWebSocket | null = null;
    ready = false;
    params: any = { };

    constructor(url: string, params = null) {
        this.url = url;
        if (params) {
            this.params = params;
        }
    }

    on_close() {
        // console.log('WS Closed')
    }

    init_receiver(handler: any) {
        if (!this.ready || !this.ws) {
            this.ws = this.reconnect()
        }
        this.ws.onclose = this.on_close;
        this.ws.onmessage = handler;
    }

    reconnect() {
        if (this.ws) {
            console.log('##################### DISCONNECT WS [' + this.url + ']')
            this.disconnect()
        }

        this.ws = new ReconnectingWebSocket(this.url, undefined, this.params);
        this.ws.onopen = () => {
            this.ready = true;
            console.log('WS opened: [' + this.url + ']');
        };
        return this.ws;
    }

    disconnect() {
        if (this.ws) {
            this.ws.close();
            this.ws = null;
            this.ready = false
        }
        console.log('WS Disconnected: [' + this.url + ']')
    }

    send_event(data: any) {
        if (data) {
            if (!this.ready || !this.ws) {
                this.ws = this.reconnect()
            }
            this.ws.send(JSON.stringify(data));
            console.log('message sent: ', data);
        }
    }

    send (data: any) {
        this.send_event(data);
    }

}
